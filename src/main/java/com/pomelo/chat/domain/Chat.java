package com.pomelo.chat.domain;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

@Data
public class Chat implements Serializable {
    /**
     * 对话id
     */
    private Integer chatId;

    /**
     * 用户名
     */
    private String username;

    /**
     * 用户api-key
     */
    private String userKey;

    /**
     * 使用的model类型
     */
    private String model;

    /**
     * 问题
     */
    private String prompt;

    /**
     * 答案
     */
    private String answer;

    /**
     * 是否结束
     */
    private Boolean isEnd;

    /**
     * 用户ip地址
     */
    private String requestIpAddress;

    /**
     * 返回的responseJSON串
     */
    private String responseJson;

    private String promptId;

    /**
     * 问答结束的原因
     */
    private String finishReason;

    /**
     * 请求的url
     */
    private String requestUrl;

    /**
     * 请求创建的时间
     */
    private Date createTime;

    /**
     * 请求完成的时间
     */
    private Long requestTime;

    /**
     * 要生成的最大字符数
     */
    private Integer maxTokens;

    /**
     * 采样温度
     */
    private Double temperature;

    /**
     * 运行的线程名称
     */
    private String threadName;

    @Serial
    private static final long serialVersionUID = 1L;
}