package com.pomelo.chat.controller;

import com.pomelo.chat.domain.ChatVo;
import com.pomelo.chat.service.ChatGPTService;
import com.pomelo.chat.service.ChatService;
import com.pomelo.chat.util.AjaxResult;
import com.pomelo.chat.util.Common;
import com.pomelo.chat.util.Question;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/open-ai")
@Slf4j
public class ChatController {

    @Resource
    private ChatGPTService chatGPTService;

    @Resource
    private ChatService chatService;

    /**
     * 使用 SSEEmitter 实现服务端推送 模型 gpt-3.5-turbo
     * @param question
     * @param request
     * @return
     * @throws IOException
     */
    @GetMapping("/chat")
    public SseEmitter chat(@RequestParam("question")String question, HttpServletRequest request) throws IOException {
        return chatGPTService.askChatGPT35(question, request);
    }

    /**
     * 查询历史记录
     * @param username 用户名
     * @param pageSize 查询记录条数
     * @param pageNum 查询记录页数
     */
    @GetMapping("/history")
    public AjaxResult history(String username, Integer pageSize, Integer pageNum) {
        if (username == null) {
            username = "name";
        }
        List<ChatVo> chats = chatService.getPage(username,pageSize, pageNum);
        return chats == null ? AjaxResult.failure("查询失败") : AjaxResult.success(chats);
    }

}
