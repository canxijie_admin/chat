package com.pomelo.chat.service.impl;

import com.pomelo.chat.domain.ChatVo;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.pomelo.chat.mapper.ChatMapper;
import com.pomelo.chat.domain.Chat;
import com.pomelo.chat.service.ChatService;

import java.util.List;

@Service
public class ChatServiceImpl implements ChatService{

    @Resource
    private ChatMapper chatMapper;

    @Override
    public int deleteByPrimaryKey(Integer chatId) {
        return chatMapper.deleteByPrimaryKey(chatId);
    }

    @Override
    public int insert(Chat record) {
        return chatMapper.insert(record);
    }

    @Override
    public int insertSelective(Chat record) {
        return chatMapper.insertSelective(record);
    }

    @Override
    public Chat selectByPrimaryKey(Integer chatId) {
        return chatMapper.selectByPrimaryKey(chatId);
    }

    @Override
    public int updateByPrimaryKeySelective(Chat record) {
        return chatMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Chat record) {
        return chatMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<Chat> getAllByUsername(String username) {
        return chatMapper.selectByUsername(username);
    }

    @Override
    public List<ChatVo> getPage(String username, Integer pageSize, Integer pageNum) {
        if (pageSize == null) {
            pageSize = 10;
        }
        if (pageNum == null) {
            pageNum = 0;
        }
        Integer start = pageNum * pageSize;
        List<Chat> chats = chatMapper.selectPage(username, start, pageSize);
        if (chats == null) {
            return null;
        }
        return ChatVo.toChatVo(chats);
    }

}
