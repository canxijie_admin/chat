# chat-gpt3

#### 介绍

使用java连接chatGPT 

[api介绍](https://platform.openai.com/docs/api-reference/introduction)

[Community libraries](https://platform.openai.com/docs/libraries/community-libraries)

[openai-java](https://github.com/TheoKanning/openai-java)

[chatgpt-java](https://github.com/Grt1228/chatgpt-java)


#### 使用说明

1. 所有配置字段在`application.yml` 文件中。修改 token 和 proxy_port 为自己的

   token : openai的 api-key

   获取方式：
   登录官网[openai](https://openai.com/)，右上角Personal
   ![image-20230425103913606](https://s2.loli.net/2023/04/25/hmyFw72ZKNsCR8q.png)

   ![image-20230425104249971](https://s2.loli.net/2023/04/25/bUg1Fm9dSleM8KC.png)

2. 数据库文件在 `src/main/resources/db` 目录下，使用的是 mysql 数据库，可以使用navicat打开

3. 端口 8182

4. `/open-ai/chat`接口字段说明：

   question : 问题

5. redis 修改为自己的配置